provider "aws" {
  region     = "ap-south-1"
  profile    = "manish"
}

// create key pair
resource "aws_key_pair" "terrformkey" {
  key_name   = "terrform-key"
  public_key = file("E:/TL/hybrid-multi-cloud/terraform/aws/mypublic.key")
 }

// create sg
resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http-ssh"
  description = "Allow http and ssh inbound traffic"
  //vpc_id      = "${aws_vpc.main.id}"
  
/*
  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    //cidr_blocks = [aws_vpc.main.cidr_block]
  }

  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    //cidr_blocks = [aws_vpc.main.cidr_block]
  }
*/
egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http-ssh"
  }
}

output "my_sg_id" {
   value = aws_security_group.allow_http_ssh.id
}

// add sg rule for port no 80 and 22
resource "aws_security_group_rule" "http_rule" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_http_ssh.id
}

resource "aws_security_group_rule" "ssh_rule" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.allow_http_ssh.id
}

resource "aws_instance" "web" {
  ami           = "ami-0447a12f28fddb066"
  instance_type = "t2.micro"
  security_groups = ["allow_http-ssh"]
  key_name        =  aws_key_pair.terrformkey.key_name

  tags = {
    Name = "terraform_os"
  }
  /*
  depends_on = [
    aws_key_pair.terrformkey,
	aws_security_group.allow_http_ssh,
  ]
*/
}

output "my_ip" {
   value = aws_instance.web.public_ip
}

// create ebs volume
resource "aws_ebs_volume" "extra_hd" {
  availability_zone = aws_instance.web.availability_zone
  size              = 1

  tags = {
    Name = "Extra_ebs_volume"
  }
}

// attach ebc volume
resource "aws_volume_attachment" "ebs_attach" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.extra_hd.id
  instance_id = aws_instance.web.id
  force_detach = true
}

// ssh to ec2 os and mount the ebs volume
resource "null_resource" "nullresource_for_mount_ebs_volume" {

depends_on = [
    aws_volume_attachment.ebs_attach,
  ]
  connection {
    host = aws_instance.web.public_ip
	type = "ssh"
	user = "ec2-user"
	private_key = file("C:/Users/Manish Saini/.ssh/terrform-key.pem")
  }

  provisioner "remote-exec" {
       inline = [
	  "sudo yum install -y httpd git php",
	  "sudo systemctl restart httpd",
      "sudo systemctl enable httpd",
      "sudo mkfs.ext4  /dev/xvdh",
      "sudo mount  /dev/xvdh  /var/www/html",
      "sudo rm -rf /var/www/html/*",
	  "sudo git clone https://gitlab.com/manish1174/terraform-aws-infra/ /var/www/html"
    ]
  }
}
// create a s3 bucket with public-read acl note: bucket name should be unit.
resource "aws_s3_bucket" "mytfbucket" {
  bucket = "mytfwebbucket"
  acl    = "public-read"
  tags = {
    Name = "webbucket"
    //Environment = "Dev"
  }

provisioner "local-exec" {
	command =  "git clone https://gitlab.com/manish1174/aws-web-imges.git"
  }

provisioner "local-exec" {
    when = destroy
	command =  "echo Y | rmdir /s aws-web-imges"
  }
}

// for cloud front
locals {
  s3_origin_id = "mytfS3Origin"
}
// crete object in s3
resource "aws_s3_bucket_object" "image-object" {
  bucket = aws_s3_bucket.mytfbucket.bucket
  key    = "my-object"
  source = "aws-web-imges/aws-terraform.jpeg"
  acl    = "public-read"
// etag makes the file update when it changes; see https://stackoverflow.com/questions/56107258/terraform-upload-file-to-s3-on-every-apply
  //etag   = filemd5("aws-web-imges/")
}

// cloud front wirh s3 origin
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.mytfbucket.bucket_regional_domain_name
    origin_id   = local.s3_origin_id

	/*
    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/ABCDEFG1234567"
    }
	*/
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "this is cloudfornt which is created by tf"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # Cache behavior with precedence 0
  ordered_cache_behavior {
    path_pattern     = "/content/immutable/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }
  
  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["US", "CA", "GB", "DE", "IN"]
    }
  }

  tags = {
    Environment = "Uat"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}